node default {
    include cron-puppet
}
user { 'gareth':
    home => '/home/gareth',
    uid => '100',
    ensure => 'absent',
    comment => 'Gareth Morris',
    gid => '1000',
    shell => '/bin/bash',
    groups => ['puppet']
    }
