node "ip-172-31-32-46.us-west-2.compute.internal" {

file { '/root/example_file.txt':
    ensure => "file",
    owner  => "root",
    group  => "root",
    mode   => "700",
    content => "Congratulations!
Puppet has created this file.
",}

} # End node ip-172-31-32-46.us-west-2.compute.internal
